<?php
// start the session
// enable to keep data
// ob_start();
session_start();


// include connection
// so we can easily manage and connect to server
include("connection.php");


// we need to process the user when come into login, but they are still login
// we gonna check it first
if (isset($_SESSION["email"]) && isset($_SESSION["password"])) {
  // session alreay setted, we are going to show
  $emailSession = $_SESSION["email"];
  $passwordSession = $_SESSION["password"];

  // we gonna check the database, this user still exist or not
  $adminChekQuery = "SELECT * FROM admins WHERE email='$emailSession' AND password='$passwordSession'";
  $result = $connection->query($adminChekQuery);

  if ($result->num_rows > 0) {
    // still exist in database, we can going to dashboard
    // ok, after finish let's bring into dashboard
    header("location:dashboard.php");
  }
}

// predefine variables
// use to indicate the signup process
// this can be failed, error, or success
$processResult = '';
$processResultMessage = '';

// define the data
// we only get the form data when start create 
if (isset($_POST["action"])) {
  // if action is create and then get the 
  // form data
  if ($_POST["action"] == "login") {
    $email = $_POST["email"];
    $password = $_POST["password"];

    // to reduce data duplication
    // we need to check the data first by using their email and username
    $adminChekQuery = "SELECT * FROM admins WHERE email='$email'";
    $result = $connection->query($adminChekQuery);

    // check the size, if exist this mean
    // admin already exist
    if ($result->num_rows < 0) {
      // oppp, no admin found,
      $processResult = "failed";
      $processResultMessage = "Opps, Email ini belum terdaftar, silahkan melakukan pendaftaran";
    } else {
      $rowData = $result->fetch_assoc();


      // alright, we found the user admin
      // but we need to check the password validity
      if ($password == $rowData["PASSWORD"]) {
        // ok, the password match, so we can go further
        // before we go, we need to set session to save data into local db
        // we can use it later
        $_SESSION["email"] = "$email";
        $_SESSION["password"] = $rowData["PASSWORD"];

        // ok, after finish let's bring into dashboard
        header("location:dashboard.php");
      } else {
        // opps, password incorrect
        $processResult = "failed";
        $processResultMessage = "Opps, Silahkan masukkan password yang benar, sedikit lagi";
      }
    }
  }
}
?>


<!DOCTYPE html>
<html lang="en" class="scroll-smooth">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Media Net Solution - Sign In</title>
  <meta name="description" content="Media N et Solution, ISP, Provider">
  <link rel="shortcut icon" href="/assets/images/logo.png" type="image/x-icon">
  <link rel="stylesheet" href="/assets/styles/main.css">
</head>

<body class="font-sans font-normal text-black leading-8">
  <!-- inlcude the menu  of header-->
  <!-- use to navigate the user -->
  <!-- allow user to know everthing inside -->
  <?php include("components/menu-header.php") ?>

  <main class="main main-content home container mx-auto">
    <section class="hero-section px-10 mt-40 mb-80">
      <div class="wrapper bg-gray-100 rounded-3xl px-20 py-20 flex relative">
        <div class="left-content flex flex-col justify-start items-start w-7/12">
          <h2 class="font-bold text-7xl text-black leading-normal">Masuki Duniamu Sekarang</h2>
          <span class="text-black font-medium mt-10">Jangan tunda lagi, Ayo bangun, dan bikin semuanya jadi lebih mudah dan laju secepat kilat tanpa kompromi dengan internet kamu</span>

          <!-- sign up form -->
          <form action="" method="POST" class="flex flex-col mt-10 space-y-6 w-10/12">
            <!-- hidden data will be stored -->
            <input name="action" value="login" hidden />
            <label class="group flex flex-col">
              <input type="email" name="email" value="<?php echo $email ?? '' ?>" class="h-12 flex justify-center items-center rounded-2xl bg-white px-6 grow placeholder:text-sm text-gray-700 outline-none ring-2 ring-gray-100" placeholder="Masukkan alamat email kamu" required />
            </label>
            <label class="group flex flex-col">
              <input type="password" name="password" value="<?php echo $password ?? '' ?>" class="h-12 flex justify-center items-center rounded-2xl bg-white px-6 grow placeholder:text-sm text-gray-700 outline-none ring-2 ring-gray-100" placeholder="Masukkan password kamu" required />
            </label>

            <!-- only show when username already exist -->
            <?php if ($processResult == 'failed') { ?>
              <div class="flex error-message">
                <span class="text-red-500 hover:scale-110 animate-bounce"><?php echo $processResultMessage ?></span>
              </div>

            <?php } ?>

            <div class="flex">
              <button class="action bg-black mt-14 w-full transition-all duration-1000 hover:scale-110 hover:-translate-y-5 rounded-3xl px-7 py-5 flex justify-center items-center text-white text-sm font-medium">Masuk Sekarang</button>
            </div>
          </form>


        </div>

        <div class="right-content w-5/12">
          <img src="assets/images/saly-6.png" alt="" class="absolute -right-20 top-20 w-7/12 transition-all duration-1000 hover:scale-110 hover:-translate-y-5">
        </div>
      </div>
    </section>

  </main>


  <!-- add some footer -->
  <!-- simple footer -->
  <?php include("components/footer.php") ?>



  <!-- add javascript -->
  <!-- jquery javascript library -->
  <script src="https://code.jquery.com/jquery-3.6.0.slim.min.js" integrity="sha256-u7e5khyithlIdTpu22PHhENmPcRdFiHRjhAuHcs05RI=" crossorigin="anonymous"></script>

  <!-- main configuration javascript -->
  <script src="/assets/js/main.js">
  </script>

</body>

</html>