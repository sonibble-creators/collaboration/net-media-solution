<?php
// start the session
// enable to keep data
// ob_start();
session_start();

// include connection
// so we can easily manage and connect to server
include("connection.php");

// redirect if session was not found
if (!isset($_SESSION["email"]) || !isset($_SESSION["password"])) {
  header("location:signin.php");
}


// we will get the type of menu will rendering
$menuType = 'account';


// we gonna check the url params about the
// menu active
// check the menu variable are not null
if (isset($_REQUEST["menu"])) {
  // ok the menu was setted
  $menuType = $_REQUEST["menu"];
}



// handle other event 
// when edit, and create
// indicate by action 
if (isset($_REQUEST["action"])) {
  // now the form running action here
  $action = $_REQUEST["action"] ?? "";

  // logout 
  if ($action == 'logout') {
    session_destroy();

    header("location:dashboard.php");
  }

  // doing validation
  if ($action == 'updateAccount') {
    // before we jump into save data
    // we need to update the photo profile
    $target_dir = "storages/";
    $target_file = $_REQUEST["avatar"] ?? '';
    if (isset($_FILES["avatar-file-upload"]["name"]) && $_FILES["avatar-file-upload"]["name"] != '') {
      $target_file = $target_dir . basename($_FILES["avatar-file-upload"]["name"]);
      $imageFileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));

      // move the image
      move_uploaded_file($_FILES["avatar-file-upload"]["tmp_name"], $target_file);
    }


    // run update account
    // get some data and go
    $id = $_REQUEST["id"];
    $name = $_REQUEST["name"];
    $username = $_REQUEST["username"];
    $email = $_REQUEST["email"];
    $password = $_REQUEST["password"];

    // run update account
    $updateAccountQuery = "UPDATE admins SET name='$name', username='$username', email='$email', avatar='$target_file', password='$password'  WHERE id='$id'";
    $updateAccountResult = $connection->query($updateAccountQuery);

    // we need to save the current
    // account settings
    $_SESSION["email"] = "$email";
    $_SESSION["password"] = "$password";

    header("location:dashboard.php");
  }


  // delete package
  if ($action == 'delete-package') {
    $id = $_REQUEST["id"];

    // run remove
    $deletePackageQuery = "DELETE FROM packages WHERE id='$id'";
    $connection->query($deletePackageQuery);
  }

  // create update package
  if ($action == 'update-create-package') {
    $id = $_REQUEST["id"];
    $name = $_REQUEST["name"];
    $price = $_REQUEST["price"];
    $desc = $_REQUEST["desc"];


    // we need to check create or update
    if (isset($id) && $id != '') {
      // update
      $updateCreatePackageQuery = "UPDATE packages SET name='$name', price='$price', description='$desc' WHERE id='$id'";
    } else {
      // create
      $updateCreatePackageQuery = "INSERT INTO packages SET name='$name', price='$price', description='$desc'";
    }
    $connection->query($updateCreatePackageQuery);
  }



  // delete customer
  if ($action == 'delete-customer') {
    $id = $_REQUEST["id"];

    // run remove
    $deleteCustQuery = "DELETE FROM customers WHERE id='$id'";
    $connection->query($deleteCustQuery);
  }

  // create update package
  if ($action == 'update-create-customer') {
    // before we jump into save data
    // we need to update the photo profile
    $target_dir = "storages/";
    $target_file = $_REQUEST["customer-avatar"] ?? '';
    if (isset($_FILES["customer-file-upload"]["name"]) && $_FILES["customer-file-upload"]["name"] != '') {
      $target_file = $target_dir . basename($_FILES["customer-file-upload"]["name"]);
      $imageFileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));

      // move the image
      move_uploaded_file($_FILES["customer-file-upload"]["tmp_name"], $target_file);
    }



    $id = $_REQUEST["id"];
    $name = $_REQUEST["name"];
    $username = $_REQUEST["username"];


    // we need to check create or update
    if (isset($id) && $id != '') {
      // update
      $updateCreateCustQuery = "UPDATE customers SET name='$name', username='$username', avatar='$target_file' WHERE id='$id'";
    } else {
      // create
      $updateCreateCustQuery = "INSERT INTO customers SET name='$name', username='$username', avatar='$target_file'";
    }
    $connection->query($updateCreateCustQuery);
  }
}



// we will fetch data in each menu activated
// account
if ($menuType == 'account') {
  // fetching admin account data
  // session alreay setted, we are going to show
  $email = $_SESSION["email"];
  $password = $_SESSION["password"];

  // we gonna check the database, this user still exist or not
  $adminChekQuery = "SELECT * FROM admins WHERE email='$email' AND password='$password'";
  $accountResult = $connection->query($adminChekQuery);

  if ($accountResult->num_rows > 0) {
    // the account exist,
    // now manipulate the data
    $accountData = $accountResult->fetch_assoc();
  }
}
// package list
if ($menuType == 'package-list') {
  // fetching package list
  $packageListQuery = "SELECT * FROM packages";
  $packageListResult = $connection->query($packageListQuery);
}


// customer
if ($menuType == 'customer') {
  // fetching package customer
  $customersQuery = "SELECT * FROM customers";
  $customersResult = $connection->query($customersQuery);
}
?>


<!DOCTYPE html>
<html lang="en" class="scroll-smooth">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Media Net Solution - Dashboard</title>
  <meta name="description" content="Media N et Solution, ISP, Provider">
  <link rel="shortcut icon" href="/assets/images/logo.png" type="image/x-icon">
  <link rel="stylesheet" href="/assets/styles/main.css">

</head>


<body class="font-sans font-normal text-black leading-8">
  <!-- inlcude the menu  of header-->
  <!-- use to navigate the user -->
  <!-- allow user to know everthing inside -->
  <?php include("./components/menu-header.php") ?>

  <main class="main main-content home container mx-auto">
    <div class="wrapper flex gap-14 px-10 mt-40 my-80">
      <div class="wrapper w-3/12">
        <div class="menu bg-white border border-gray-100 rounded-2xl px-6 py-5">
          <span class="text-lg font-medium text-black">Menu</span>
          <ul class="list-none mt-5 flex flex-col gap-y-3">
            <li class="font-medium text-sm <?php echo $menuType == 'account' ? 'text-gray-50 bg-black' : 'bg-gray-50 text-black' ?> h-14 rounded-xl transition-all duration-1000  px-4 hover:bg-black hover:text-gray-50 flex items-center dash-menu-item" data-action="account">Akun Kamu</li>
            <li class="font-medium text-sm <?php echo $menuType == 'package-list' ? 'text-gray-50 bg-black' : 'bg-gray-50 text-black' ?> h-14 rounded-xl transition-all duration-1000  px-4 hover:bg-black hover:text-gray-50 flex items-center dash-menu-item" data-action="package-list">Paket Harga</li>
            <li class="font-medium text-sm <?php echo $menuType == 'customer' ? 'text-gray-50 bg-black' : 'bg-gray-50 text-black' ?> h-14 rounded-xl transition-all duration-1000  px-4 hover:bg-black hover:text-gray-50 flex items-center dash-menu-item" data-action="customer">Pengguna</li>
          </ul>
        </div>
      </div>


      <div class="content main-content w-9/12">

        <!-- run conditional rendering -->
        <!-- Account menu -->
        <?php if ($menuType == 'account') { ?>

          <div class="account-content flex flex-col">
            <h2 class="headline font-bold text-3xl">Akun Kamu</h2>

            <form action="" method="POST" class="mt-10 flex flex-col space-y-5" enctype="multipart/form-data">

              <div class="group flex flex-col">
                <!-- we have hidden data -->
                <input name="menu" value="<?php echo $menuType ?>" hidden />
                <input name="action" value="updateAccount" hidden />
                <input name="id" value="<?php echo $accountData['ID'] ?? 0 ?>" hidden />

                <span class="font-medium text-sm">Ubah Photo Profil</span>
                <div class="flex gap-x-4 mt-8 items-center">
                  <input name="avatar" hidden value="<?php echo isset($accountData["AVATAR"]) && $accountData["AVATAR"] != '' ? $accountData["AVATAR"] : '/assets/images/saly-1.png' ?>" />
                  <img class="h-20 w-20 border border-gray-100 rounded-full" src="<?php echo isset($accountData["AVATAR"]) && $accountData["AVATAR"] != '' ? $accountData["AVATAR"] : '/assets/images/saly-1.png' ?>" />
                  <input type="file" name="avatar-file-upload" class="text-sm text-slate-500 file:mr-4 file:py-2 file:px-4 file:rounded-full file:border-0 file:text-sm file:font-semibold file:bg-violet-50 file:text-violet-700 hover:file:bg-violet-100" />
                </div>
              </div>

              <label class="group flex flex-col space-y-2">
                <span class="font-medium text-sm">Nama Lengkap</span>
                <input type="text" name="name" value="<?php echo $accountData['NAME'] ?? '' ?>" class="h-12 flex justify-center items-center rounded-2xl bg-white px-6 grow placeholder:text-sm text-gray-700 outline-none ring-2 ring-gray-100" placeholder="Masukkan Nama Lengkap Kamu" />
              </label>
              <label class="group flex flex-col space-y-2">
                <span class="font-medium text-sm">Username</span>
                <input type="text" name="username" value="<?php echo $accountData['USERNAME'] ?? '' ?>" class="h-12 flex justify-center items-center rounded-2xl bg-white px-6 grow placeholder:text-sm text-gray-700 outline-none ring-2 ring-gray-100" placeholder="Masukkan Username Kamu" />
              </label>
              <label class="group flex flex-col space-y-2">
                <span class="font-medium text-sm">Email</span>
                <input type="email" name="email" value="<?php echo $accountData['EMAIL'] ?? '' ?>" class="h-12 flex justify-center items-center rounded-2xl bg-white px-6 grow placeholder:text-sm text-gray-700 outline-none ring-2 ring-gray-100" placeholder="Masukkan Username Kamu" />
              </label>
              <label class="group flex flex-col space-y-2">
                <span class="font-medium text-sm">Password</span>
                <input type="password" name="password" value="<?php echo $accountData['PASSWORD'] ?? '' ?>" class="h-12 flex justify-center items-center rounded-2xl bg-white px-6 grow placeholder:text-sm text-gray-700 outline-none ring-2 ring-gray-100" placeholder="Masukkan password Kamu" />
              </label>

              <div class="flex justify-end">
                <button type="submit" class=" mt-10 action call-to-action bg-black transition-all duration-1000 hover:scale-110 hover:-translate-y-5 rounded-3xl px-7 py-5 flex justify-center items-center text-white text-sm font-medium">Ubah Akun</button>
              </div>

            </form>

            <div class="flex justify-end">
              <button class=" mt-10 action call-to-action bg-gray-300 transition-all duration-1000 hover:scale-110 hover:-translate-y-5 rounded-3xl px-7 py-5 flex justify-center items-center text-black text-sm font-medium" onclick="location.href='dashboard.php?action=logout'">Logout</button>
            </div>
          </div>

        <?php } ?>


        <!-- Package List Menu -->
        <!-- use to show the packages prices -->
        <?php if ($menuType == 'package-list') { ?>

          <div class="account-content flex flex-col">
            <div class="flex items-center">
              <h2 class="headline font-bold text-3xl">Semua Paket</h2>
              <div class="grow"></div>
              <button class="cta font-medium text-sm  text-gray-50 bg-black rounded-2xl py-5 px-7 btn-new-package">Buat Paket Harga</button>
            </div>


            <div class="wrapper grid grid-cols-3 gap-6 mt-10">

              <!-- show the package items -->
              <?php
              if (isset($packageListResult)) {
                while ($rowPackage = $packageListResult->fetch_array()) {
              ?>

                  <div class="price-item flex flex-col border border-gray-100 rounded-2xl px-5 py-5 transition-all duration-1000 hover:-translate-y-3 hover:scale-x-110 hover:bg-gray-50">
                    <div class="price-heading flex-col border-b border-gray-100 py-2">
                      <h4 class="font-medium text-black text-lg"><?php echo $rowPackage["NAME"] ?? '' ?></h4>
                      <span class="text-black font-bold text-4xl">Rp. <?php echo number_format($rowPackage["PRICE"] ?? 0.0, 2, ',', '.') ?></span>
                    </div>

                    <div class="features flex flex-col space-y-2 mt-4 ml-2">
                      <span class="text-gray-800"><?php echo $rowPackage["DESCRIPTION"] ?? '' ?></span>
                    </div>
                    <div class="grow"></div>
                    <div class="flex gap-2 mt-8">
                      <button class="cta font-medium text-sm text-gray-50 bg-black rounded-xl py-2 px-4 btn-update-package" data-menu="<?php echo $menuType ?>" data-id="<?php echo $rowPackage["ID"] ?>" data-name="<?php echo $rowPackage["NAME"] ?>" data-price="<?php echo $rowPackage["PRICE"] ?>" data-desc="<?php echo $rowPackage["DESCRIPTION"] ?>">Ubah</button>
                      <button class="cta font-medium text-sm text-gray-50 bg-black rounded-xl py-2 px-4 btn-remove-package" data-menu="<?php echo $menuType ?>" data-id="<?php echo $rowPackage["ID"] ?>">Hapus</button>
                    </div>

                  </div>


              <?php
                }
              }
              ?>
            </div>
          </div>

        <?php } ?>


        <!-- Costumer Menu -->
        <!-- use to show the packages prices -->
        <?php if ($menuType == 'customer') { ?>

          <div class="account-content flex flex-col">
            <div class="flex items-center">
              <h2 class="headline font-bold text-3xl">Pengguna</h2>
              <div class="grow"></div>
              <button class="cta font-medium text-sm  text-gray-50 bg-black rounded-2xl py-5 px-7 btn-new-customer">Tambah Pengguna</button>
            </div>
            <div class="wrapper grid grid-cols-3 gap-6 mt-10">

              <!-- show customers data -->

              <?php
              if (isset($customersResult)) {
                while ($rowCustomer = $customersResult->fetch_array()) {
              ?>

                  <div class="customer-item p-5 rounded-2xl border border-gray-100 flex flex-col">
                    <div class="heading flex space-x-4">
                      <img src="<?php echo isset($rowCustomer["AVATAR"]) && $rowCustomer["AVATAR"] != '' ? $rowCustomer["AVATAR"] : '/assets/images/saly-1.png' ?>" alt="" class="object-cover h-14 w-14 rounded-full border border-gray-100 p-2">
                      <div class="customer-detail flex flex-col grow space-y-1">
                        <span class="font-bold"><?php echo $rowCustomer["NAME"] ?? '' ?></span>
                        <span class="text-gray-600 text-sm">@<?php echo $rowCustomer["USERNAME"] ?? '' ?></span>
                        <span class="text-gray-600 text-sm"><?php echo date_format(date_create($rowCustomer["CREATED_AT"]), 'l, d F Y') ?></span>
                      </div>
                    </div>
                    <div class="grow"></div>
                    <div class="flex mt-8 gap-3">
                      <button class="cta font-medium text-sm text-gray-50 bg-black rounded-xl py-2 px-4 btn-update-customer" data-menu="<?php echo $menuType ?>" data-id="<?php echo $rowCustomer["ID"] ?>" data-name="<?php echo $rowCustomer["NAME"] ?>" data-username="<?php echo $rowCustomer["USERNAME"] ?>" data-avatar="<?php echo isset($rowCustomer["AVATAR"]) && $rowCustomer["AVATAR"] != '' ? $rowCustomer["AVATAR"] : '/assets/images/saly-1.png' ?>">Ubah</button>
                      <button class="cta font-medium text-sm text-gray-50 bg-black rounded-xl py-2 px-4 btn-remove-customer" data-menu="<?php echo $menuType ?>" data-id="<?php echo $rowCustomer["ID"] ?>">Hapus</button>
                    </div>
                  </div>

              <?php
                }
              }
              ?>


            </div>
          </div>

        <?php } ?>

      </div>
    </div>
  </main>


  <!-- add some footer -->
  <!-- simple footer -->
  <?php include("components/footer.php") ?>



  <!-- Package Detail Modal -->
  <!-- show the detail of package and enable to doing create and update -->
  <div class="modal relative hidden" id="package-detail-modal">
    <div class="modal-background fixed inset-0 bg-black bg-opacity-30 w-screen h-screen"></div>
    <div class="modal-wrapper fixed z-30 w-8/12 inset-0 mx-auto h-screen overflow-y-auto">
      <div class="modal-content bg-white rounded-2xl p-10 w-full my-20">
        <div class="content flex flex-col">
          <h2 class="headline font-bold text-black text-3xl">Paket</h2>


          <form action="" method="POST" class="login-form flex flex-col mt-10 space-y-6">

            <!-- hidden input -->
            <input name="menu" value="<?php echo $menuType ?>" hidden />
            <input name="action" value="update-create-package" hidden />
            <input name="id" value="" hidden id="upcreid" />


            <label class="group flex flex-col space-y-2">
              <span class="font-medium text-gray-500 text-sm ml-2">Nama Paket</span>
              <input type="text" name="name" id="upcrename" class="h-12 flex justify-center items-center rounded-2xl bg-white px-6 grow placeholder:text-sm text-gray-700 outline-none ring-2 ring-gray-100" placeholder="Nama Paket Kamu" />
            </label>
            <label class="group flex flex-col space-y-2">
              <span class="font-medium text-gray-500 text-sm ml-2">Harga Paket</span>
              <input type="number" name="price" id="upcreprice" class="h-12 flex justify-center items-center rounded-2xl bg-white px-6 grow placeholder:text-sm text-gray-700 outline-none ring-2 ring-gray-100" placeholder="Harga Paket Kamu" />
            </label>
            <label class="group flex flex-col space-y-2">
              <span class="font-medium text-gray-500 text-sm ml-2">Deskripsi Paket</span>
              <textarea type="text" name="desc" id="upcredesc" class="py-4 h-60 flex justify-center items-center rounded-2xl bg-white px-6 grow placeholder:text-sm text-gray-700 outline-none ring-2 ring-gray-100" placeholder="Ceritakan Paket Kamu"> </textarea>
            </label>


            <div class="flex justify-end">
              <button class="action bg-black mt-14 transition-all duration-1000 hover:scale-110 hover:-translate-y-5 rounded-3xl px-7 py-5 flex justify-center items-center text-white text-sm font-medium">Simpan</button>
            </div>
          </form>

        </div>
      </div>
    </div>
  </div>


  <div class="modal relative hidden" id="customer-detail-modal">
    <div class="modal-background fixed inset-0 bg-black bg-opacity-30 w-screen h-screen"></div>
    <div class="modal-wrapper fixed z-30 w-8/12 inset-0 mx-auto h-screen overflow-y-auto">
      <div class="modal-content bg-white rounded-2xl p-10 w-full my-20">
        <div class="content flex flex-col">
          <h2 class="headline font-bold text-black text-3xl">Pelanggan</h2>


          <form action="" method="POST" class="login-form flex flex-col mt-10 space-y-6" enctype="multipart/form-data">

            <!-- hidden input -->
            <input name="menu" value="<?php echo $menuType ?>" hidden />
            <input name="action" value="update-create-customer" hidden />
            <input name="id" value="" hidden id="upcrecusid" />

            <div class="group flex flex-col">
              <span class="font-medium text-sm">Ubah Photo Profil</span>
              <div class="flex gap-x-4 mt-8 items-center">
                <input name="customer-avatar" id="customer-avatar" hidden />
                <img class="h-20 w-20 border border-gray-100 rounded-full" id="customer-detail-image" src="/assets/images/saly-1.png" />
                <input type="file" name="customer-file-upload" class="text-sm text-slate-500 file:mr-4 file:py-2 file:px-4 file:rounded-full file:border-0 file:text-sm file:font-semibold file:bg-violet-50 file:text-violet-700 hover:file:bg-violet-100" />
              </div>
            </div>


            <label class="group flex flex-col space-y-2">
              <span class="font-medium text-gray-500 text-sm ml-2">Nama</span>
              <input type="text" name="name" id="upcrecusname" class="h-12 flex justify-center items-center rounded-2xl bg-white px-6 grow placeholder:text-sm text-gray-700 outline-none ring-2 ring-gray-100" placeholder="Nama Paket Kamu" />
            </label>
            <label class="group flex flex-col space-y-2">
              <span class="font-medium text-gray-500 text-sm ml-2">Username</span>
              <input type="text" name="username" id="upcrecususername" class="h-12 flex justify-center items-center rounded-2xl bg-white px-6 grow placeholder:text-sm text-gray-700 outline-none ring-2 ring-gray-100" placeholder="Harga Paket Kamu" />
            </label>

            <div class="flex justify-end">
              <button class="action bg-black mt-14 transition-all duration-1000 hover:scale-110 hover:-translate-y-5 rounded-3xl px-7 py-5 flex justify-center items-center text-white text-sm font-medium">Simpan</button>
            </div>
          </form>

        </div>
      </div>
    </div>
  </div>



  <!-- add javascript -->
  <!-- jquery javascript library -->
  <script src="https://code.jquery.com/jquery-3.6.0.slim.min.js" integrity="sha256-u7e5khyithlIdTpu22PHhENmPcRdFiHRjhAuHcs05RI=" crossorigin="anonymous"></script>

  <!-- main configuration javascript -->
  <script src="/assets/js/main.js">
  </script>
</body>

</html>