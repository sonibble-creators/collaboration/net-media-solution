<?php
// start the session
// enable to keep data
// ob_start();
session_start();


// include connection
// so we can easily manage and connect to server
include("connection.php");

// package list
// fetching package list
$packageListQuery = "SELECT * FROM packages";
$packageListResult = $connection->query($packageListQuery);

// fetching package customer
$customersQuery = "SELECT * FROM customers";
$customersResult = $connection->query($customersQuery);



?>


<!DOCTYPE html>
<html lang="en" class="scroll-smooth">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Media Net Solution - Tempat kamu buat nyari koneksi terbaik</title>
  <meta name="description" content="Media N et Solution, ISP, Provider">
  <link rel="shortcut icon" href="/assets/images/logo.png" type="image/x-icon">
  <link rel="stylesheet" href="/assets/styles/main.css">
</head>

<body class="font-sans font-normal text-black leading-8">
  <!-- inlcude the menu  of header-->
  <!-- use to navigate the user -->
  <!-- allow user to know everthing inside -->
  <?php include("components/menu-header.php") ?>

  <main class="main main-content home container mx-auto">
    <section class="hero-section px-10 mt-40">
      <div class="wrapper bg-orange-200 rounded-3xl px-20 py-20 flex relative overflow-hidden">
        <div class="left-content flex flex-col justify-start items-start w-7/12">
          <h2 class="font-bold text-7xl text-black leading-normal">Dapatkan Semua Kelebihan </h2>
          <span class="text-black font-medium mt-10">Kami Pastikan semuanya berjalan aman bersama koneksi tercepat kamu, Yuk ikutan gabung bareng kami</span>
          <div class="actions flex gap-4 mt-40">
            <button class="action call-to-action bg-black ttransition-all duration-1000 hover:scale-110 hover:-translate-y-5 rounded-3xl px-7 py-5 flex justify-center items-center text-white text-sm font-medium" onclick="location.href='/signup.php'">Daftar Pelanggan</button>
            <!-- <button class="action call-to-action bg-orange-500 transition-all duration-1000 hover:scale-110 hover:-translate-y-5 rounded-3xl px-7 py-5 flex justify-center items-center text-white text-sm font-medium">Daftar Paket</button> -->
          </div>
        </div>

        <div class="right-content w-5/12">
          <img src="assets/images/saly-1.png" alt="" class="absolute right-0 top-0 w-7/12 transition-all duration-1000 hover:scale-110 hover:-translate-y-5">
        </div>
      </div>
    </section>


    <!-- pricing section -->
    <!-- allow to show the pricing package,and other stuff -->
    <section class="pricing-section mt-80 px-10">
      <div class="wrapper flex flex-col items-center">
        <!-- headline -->
        <div class="headline w-7/12 flex flex-col">
          <h2 class="font-bold text-6xl text-black text-center">
            Pilih Sesuai Kebutuhan Kamu
          </h2>
          <span class="font-medium text-black text-center mt-10">Kami menyiapkan semuanya untuk kamu, pastikan semuanya berjalan aman ya. Opps, semuanya murah kok</span>
        </div>

        <div class="bottom-content grid grid-cols-3 gap-6 price-list w-10/12 mt-20">

          <!-- show the package items -->
          <?php
          if (isset($packageListResult)) {
            while ($rowPackage = $packageListResult->fetch_array()) {
          ?>

              <div class="price-item flex flex-col border border-gray-100 rounded-2xl px-5 py-5 transition-all duration-1000 hover:-translate-y-3 hover:scale-x-110 hover:bg-gray-50">
                <div class="price-heading flex-col border-b border-gray-100 py-2">
                  <h4 class="font-medium text-black text-lg"><?php echo $rowPackage["NAME"] ?? '' ?></h4>
                  <span class="text-black font-bold text-4xl">Rp. <?php echo number_format($rowPackage["PRICE"] ?? 0.0, 2, ',', '.') ?></span>
                </div>

                <div class="features flex flex-col space-y-2 mt-4 ml-2">
                  <span class="text-gray-800"><?php echo $rowPackage["DESCRIPTION"] ?? '' ?></span>
                </div>
                <div class="grow"></div>
                <div class="flex gap-2 mt-8">
                  <button class="w-full action bg-black transition-all duration-1000 hover:scale-110 hover:-translate-y-5 rounded-3xl px-7 py-5 flex justify-center items-center text-gray-50 text-sm font-medium" id="signup-action">Daftar Sekarang</button>
                </div>

              </div>


          <?php
            }
          }
          ?>

        </div>

      </div>
    </section>


    <!-- recent customer pick package -->
    <section class="customer-pick-package-section mt-80 px-10">
      <div class="wrapper flex space-x-12">
        <div class="content-left w-7/12">
          <div class="wrapper-customer grid grid-cols-2 gap-5">

            <!-- show customers data -->

            <?php
            if (isset($customersResult)) {
              while ($rowCustomer = $customersResult->fetch_array()) {
            ?>

                <div class="customer-item p-5 rounded-2xl border border-gray-100 flex flex-col">
                  <div class="heading flex items-center space-x-4">
                    <img src="<?php echo isset($rowCustomer["AVATAR"]) ? $rowCustomer["AVATAR"] : '/assets/images/saly-1.png' ?>" alt="" class="object-cover h-14 w-14 rounded-full border border-gray-100 p-2">
                    <div class="customer-detail flex flex-col grow space-y-1">
                      <span class="font-bold"><?php echo $rowCustomer["NAME"] ?? '' ?></span>
                      <span class="text-gray-600 text-sm">@<?php echo $rowCustomer["USERNAME"] ?? '' ?></span>
                      <span class="text-gray-600 text-sm"><?php echo date_format(date_create($rowCustomer["CREATED_AT"]), 'l, d F Y') ?></span>
                    </div>
                  </div>
                </div>

            <?php
              }
            }
            ?>


          </div>
        </div>
        <div class="content-right float-right w-5/12 flex flex-col">
          <h2 class="font-bold text-6xl text-black leading-[1.27]">
            Orang yang telah bergabung menjadi yang terdepan
          </h2>
          <span class="font-medium text-black mt-10">Ayo bergabung bersama orang terdepan untuk semua agar lebih cepat dan aman</span>


          <button class="action call-to-action mt-12 bg-black transition-all duration-1000 hover:scale-110 hover:-translate-y-5 rounded-3xl px-7 py-5 flex justify-center items-center text-white text-sm font-medium" onclick="location.href='/signup.php'">Gabung Sekarang</button>
        </div>
      </div>
    </section>

    <section class="mailing-section my-80 px-10">
      <div class="wrapper bg-orange-300 px-20 py-20 rounded-3xl flex flex-col relative">
        <h2 class="font-bold text-3xl text-black">Tetap Terhubung Bersama kami</h2>
        <span class="font-medium text-black mt-4">Pastikan kamu selalu update, dan nggak ketinggalan info terkini dari kami</span>
        <div class="group flex flex-row gap-8 mt-14 items-center w-8/12">
          <input type="email" class="h-14 rounded-2xl bg-white px-6 grow placeholder:text-sm text-gray-700 focus:ring-0 focus:outline-none" placeholder="Masukkan alamat email kamu" />
          <button class="action call-to-action bg-black transition-all duration-1000 hover:scale-110 hover:-translate-y-5 rounded-3xl px-7 py-5 flex justify-center items-center text-white text-sm font-medium">Berlangganan</button>
        </div>

        <!-- illustration -->
        <img src="/assets/images/saly-25.png" class="absolute -right-16 -top-20 object-cover w-6/12" />
      </div>
    </section>
  </main>


  <!-- add some footer -->
  <!-- simple footer -->
  <?php include("components/footer.php") ?>

  <!-- add javascript -->
  <!-- jquery javascript library -->
  <script src="https://code.jquery.com/jquery-3.6.0.slim.min.js" integrity="sha256-u7e5khyithlIdTpu22PHhENmPcRdFiHRjhAuHcs05RI=" crossorigin="anonymous"></script>

  <!-- main configuration javascript -->
  <script src="/assets/js/main.js">
  </script>

</body>

</html>