// run the javascript when the document already rendered
$(function () {
  // do anything here
  // come from jquery 

  // close every modal
  // when the background clicked
  $('.modal-background').click(function () {
    console.log("print");
    // hide every modal
    $('.modal').hide();
  });


  // dashboard menu event
  // when the menu clicked in dashboard
  $('.dash-menu-item').click(function () {
    // when this clicked
    // we going to move the url, and add params
    var menu = $(this).data("action");


    // passing the menu params
    location.href = "/dashboard.php?menu=" + menu;
  });


  // controll the menu header
  // going to sign up
  $('#signup-action').click(function () {
    location.href = "/signup.php";
  });

  // going to sign in
  $('#login-action').click(function () {
    location.href = "/signin.php";
  });
  // going to sign in
  $('#account-avatar-btn').click(function () {
    location.href = "/dashboard.php";
  });



  // show package modal
  $('.btn-new-package').click(function () {
    $('#package-detail-modal').show();
  });

  $('.btn-update-package').click(function () {
    // before show
    // we need to add value into form
    var id = $(this).data("id");
    var name = $(this).data("name");
    var price = $(this).data("price");
    var desc = $(this).data("desc");


    // set data into element
    $('#upcreid').val(id);
    $('#upcrename').val(name);
    $('#upcreprice').val(price);
    $('#upcredesc').val(desc);

    $('#package-detail-modal').show();
  });

  // controlling the dashboard event
  $('.btn-remove-package').click(function () {
    var menu = $(this).data("menu");
    var id = $(this).data("id");

    location.href = "/dashboard.php?menu=" + menu + "&action=delete-package&id=" + id;
  });


  // customer
  $('.btn-new-customer').click(function () {
    $('#customer-detail-modal').show();
  });

  $('.btn-update-customer').click(function () {
    // before show
    // we need to add value into form
    var id = $(this).data("id");
    var name = $(this).data("name");
    var username = $(this).data("username");
    var avatar = $(this).data("avatar");

    console.log(avatar);


    // set data into element
    $('#upcrecusid').val(id);
    $('#upcrecusname').val(name);
    $('#upcrecususername').val(username);
    $('#customer-avatar').val(avatar);
    $('#customer-detail-image').attr("src", avatar);

    $('#customer-detail-modal').show();
  });

  // controlling the dashboard event
  $('.btn-remove-customer').click(function () {
    var menu = $(this).data("menu");
    var id = $(this).data("id");

    location.href = "/dashboard.php?menu=" + menu + "&action=delete-customer&id=" + id;
  });


});