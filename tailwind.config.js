module.exports = {
  content: [
    './index.php',
    './index.{html,js,php}',
    './*.{html,js,php}',
    './components/*.{html,js,php}',
  ],
  theme: {
    fontFamily: {
      sans: ['DM Sans', 'sans-serif'],
    },
    extend: {},
  },
  plugins: [],
}
