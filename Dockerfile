FROM php:8.1.2-fpm

# Set working directory
WORKDIR /var/www/html/

# Install Extension
RUN docker-php-ext-install mysqli pdo_mysql


# Copy existing application directory contents
COPY . /var/www/html/

# Expose port 9000 and start php-fpm server
EXPOSE 9000
CMD ["php-fpm"]