<?php

// this is component
// so we don't need to initialize some method 
// like session, style and others

$adminUser;


// we need to process the user when come into login, but they are still login
// we gonna check it first
if (isset($_SESSION["email"]) && isset($_SESSION["password"])) {
  // session alreay setted, we are going to show
  $email = $_SESSION["email"];
  $password = $_SESSION["password"];

  // we gonna check the database, this user still exist or not
  $adminChekQuery = "SELECT * FROM admins WHERE email='$email' AND password='$password'";
  $result = $connection->query($adminChekQuery);
  if ($result->num_rows > 0) {
    // the data exist, we can use it actually
    $adminUser = $result->fetch_assoc();
  }
}

?>
<div class="header" id="simple-header">
  <div class="header-wrapper container mx-auto flex flex-row my-7 justify-center items-center space-x-16">
    <div class="brand">
      <a href="/" class="brand-link flex flex-row justify-center items-center space-x-3">
        <img src="/assets/images/logo.png" class="object-cover w-16 h-16 rounded-full overflow-hidden" />
        <span class="text-xl font-medium text-black">Media Net Solution</span>
      </a>
    </div>
    <div class="grow"></div>
    <?php if (isset($adminUser)) { ?>
      <div class="account flex space-x-4">
        <img src="<?php echo isset($adminUser["AVATAR"]) && $adminUser["AVATAR"] != '' ? $adminUser["AVATAR"] : '/assets/images/saly-1.png' ?> " class=" h-12 w-12 rounded-full object-cover border-2" id="account-avatar-btn" />
        <div class="detail flex flex-col">
          <span class="font-medium text-sm"><?php echo $adminUser["NAME"] ?></span>
          <span class="text-xs font-medium">@<?php echo $adminUser["USERNAME"] ?></span>
        </div>
      </div>
    <?php } else { ?>
      <div class="flex gap-6">
        <button class="action bg-gray-100 transition-all duration-1000 hover:scale-110 hover:-translate-y-5 rounded-3xl px-7 py-5 flex justify-center items-center text-black text-sm font-medium" id="signup-action">Daftar Sekarang</button>
        <button class="action bg-black transition-all duration-1000 hover:scale-110 hover:-translate-y-5 rounded-3xl px-7 py-5 flex justify-center items-center text-white text-sm font-medium" id="login-action">Masuk Sekarang</button>
      </div>

    <?php } ?>
  </div>
</div>