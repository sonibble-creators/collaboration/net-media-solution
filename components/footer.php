<div class="footer simple-footer bg-black" id="simple-footer">
  <div class="footer-wrapper container px-24 py-10 container mx-auto flex flex-col">
    <div class="credit flex">
      <span class="font-medium text-gray-50">Copyright Net Media Solution - Alright Reserved</span>
      <div class="grow"></div>
      <div class="credit-link flex space-x-6">
        <a href="/" target="_blank" class="font-medium text-gray-50">Home</a>
        <a href="/signin.php" target="_blank" class="font-medium text-gray-50">Sign In</a>
        <a href="/signup.php" target="_blank" class="font-medium text-gray-50">Sign Up</a>
        <a href="/dashboard.php" target="_blank" class="font-medium text-gray-50">Dashboard</a>
      </div>
    </div>
  </div>
</div>