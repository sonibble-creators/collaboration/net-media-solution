<?php
// configuration for connection into 
// database server
// will contain many secret data

// remember to customize with your own
// configuration environment
$dbhost = "db";
$dbuser = "root";
$dbpass = "vibesonly15";
$dbport = 3306;
$dbname = "medianetsolution";

// we need to define the connection
// using mysqli
$connection = new mysqli($dbhost, $dbuser, $dbpass, $dbname, $dbport);

// ok, now the connection war ready, we need to check the connection
// if it's run or there's some problem
if ($connection->connect_errno) {
  // we will die the connection, and show some error message
  die("Unable to connect with database cause : " . $connection->connect_error);
}

// ok, it's ready
